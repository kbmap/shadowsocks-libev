# shadowsocks-libev

Always up-to-date [shadowsocks-libev](https://github.com/shadowsocks/shadowsocks-libev) repository.


## How to install for **Debian/Ubuntu/Linux Mint**

* Install required packages

    ```sh
    sudo apt install --no-install-recommends apt-transport-https ca-certificates wget
    ```

* Add the repository

    ```sh
    sudo wget -qO /etc/apt/trusted.gpg.d/test-public.gpg https://kbmap.gitlab.io/shadowsocks-libev/test-public.gpg
    echo "deb https://kbmap.gitlab.io/shadowsocks-libev/<os-id>/ <codename> main" | sudo tee /etc/apt/sources.list.d/shadowsocks-libev.list
    ```

    * You may replace `<os-id>` with the distributor ID according to your system. It supports `debian` and `ubuntu`.

    * You may replace `<codename>` with the code name of the distribution according to your system. It supports `stretch`, `buster`, `xenial`, `bionic` and `disco`.

* Install shadowsocks-libev package

    ```sh
    sudo apt update && sudo apt install shadowsocks-libev
    ```


## Packages for another Linux distributions

Just make a pull request.
