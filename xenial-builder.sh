#! /bin/sh

set -ex

# docker run --name xx --rm -it -v $PWD:/x:ro -w /build ubuntu:xenial

VER_MBEDTLS="2.16.1"
VER_SODIUM="1.0.17"
CFLAGS="-Os"
LDFLAGS="-static -s"

apt-get update
apt-get install -y --no-install-recommends curl ca-certificates git build-essential autoconf automake libtool libpcre3-dev asciidoc xmlto libc-ares-dev libev-dev pkg-config
apt-get install -t xenial-backports -y --no-install-recommends debhelper

curl -sSL https://github.com/ARMmbed/mbedtls/archive/mbedtls-$VER_MBEDTLS.tar.gz | tar xz
cd mbedtls-mbedtls-$VER_MBEDTLS
make -j $(nproc)
cd -

curl -sSL https://github.com/jedisct1/libsodium/releases/download/$VER_SODIUM/libsodium-$VER_SODIUM.tar.gz | tar xz
cd libsodium-$VER_SODIUM
./configure --prefix=/usr --enable-shared=false
make -j $(nproc)
cd -

git clone --recursive https://github.com/shadowsocks/shadowsocks-libev.git
mbedtls_root="$(realpath $PWD/mbedtls-mbedtls-$VER_MBEDTLS)"
libsodium_root="$(realpath $PWD/libsodium-$VER_SODIUM/src/libsodium)"
cd shadowsocks-libev
sed -e 's/^ \(libmbedtls-dev,\)/#\1/' \
	-e 's/^ \(libsodium-dev\)/#\1/' \
	-e '43,$d' \
	-i debian/control
sed -e "s|^\t\t--enable-shared|\t\t--with-mbedtls-include=$mbedtls_root/include --with-mbedtls-lib=$mbedtls_root/library --with-sodium-include=$libsodium_root/include --with-sodium-lib=$libsodium_root/.libs --enable-static|" \
	-i debian/rules
sed -e '/^usr\/bin\/$/d' \
	-e '/^usr\/share\/man\/$/d' \
	-i debian/shadowsocks-libev.install
# rm debian/libshadowsocks-libev2.install debian/libshadowsocks-libev-dev.install
echo export LDFLAGS = "-L$mbedtls_root/library -L$libsodium_root/.libs" >>debian/rules
./autogen.sh
dpkg-buildpackage -b -us -uc -j$(nproc)
cd -
